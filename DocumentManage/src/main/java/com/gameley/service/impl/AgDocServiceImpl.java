package com.gameley.service.impl;

import com.gameley.entity.AgDoc;
import com.gameley.dao.AgDocDao;
import com.gameley.service.AgDocService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WWMXD
 * @since 2018-01-30 14:46:58
 */
@Service
public class AgDocServiceImpl extends ServiceImpl<AgDocDao, AgDoc> implements AgDocService  {
	
}
