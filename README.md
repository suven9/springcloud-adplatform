###  **Spring Cloud 项目脚手架** 
自动生成测试网址：http://dota2info.cn/index

(如需修改配置，只需修改config-repo中的generator.xml，项目会采用springcloud-bus来进行动态更新)

 **技术栈** 
1. Spring Boot
1. MySQL
1. Spring Cloud
1. Mybatis
1. Mybatis-Plus
1. Swagger2

 **项目介绍** 

持久层采用mybatis持久化，使用MyBatis-Plus优化，减少sql开发量；

前台代码采用elementui,对单表的增删改查页面（包含分页）可以通过脚手架一键生成，简化90%的代码开发；

日志记录目前采用logback，后续会使用elk，对日志进行统计分析；

Druid数据库连接池，监控数据库访问性能，统计SQL的执行性能；

后台采用springboot-admin进行监控；

swagger生成接口文档；

**初步搭建，还在修改** 

 **前台页面（初步测试开发）**
![输入图片说明](https://gitee.com/uploads/images/2018/0118/183152_ec0b7742_1463938.png "深度截图_选择区域_20180118183119.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0118/125531_0f7ecd32_1463938.png "深度截图_选择区域_20180118125327.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0105/192313_5b896fbe_1463938.png "前端UI.png")

 **前台安装步骤（需要安装node.js）**
1. cd web
1. npm install 
1. npm run dev


 **更新内容**

_#2018-02-01_ 

  _增加elk docker镜像，目前可以导入nginx的日志，进行分析。elk-kafka 为后边数据统计导入做准备_  